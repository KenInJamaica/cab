package main;

import GUI.CabGUI;
import cabDatabase.SQLProvider;

public class Driver {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		SQLProvider.getConnection();
		SQLProvider.createTable();
		new CabGUI();
	}

}
