package GUI;

import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import cabDatabase.SQLProvider;

import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import java.awt.Color;

public class Database_GUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Database_GUI frame = new Database_GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Database_GUI() {
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 511, 578);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		final JTextArea textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setBounds(10, 47, 475, 268);
		contentPane.add(textArea);
		final Runnable runnable = (Runnable) Toolkit.getDefaultToolkit().getDesktopProperty("win.sound.exclamation");
		
		JButton btnSummary = new JButton("SUMMARY");
		btnSummary.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
				String a, str;
				
					a = SQLProvider.getAvailability();
					str = SQLProvider.getSummary(a);
					textArea.setText("\tBelow shows the summary of each Taxi Operator\n");
					textArea.append("-------------------------------------------------------------"
							+ "-------------------------------------------------------------\n");
					textArea.append("CarID\t\tTotal Distance\t\tTotal Amount\n");
					textArea.append("-------------------------------------------------------------"
							+ "-------------------------------------------------------------\n");
					textArea.append(str);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		btnSummary.setBounds(76, 341, 111, 23);
		contentPane.add(btnSummary);
	
		JButton btnUnfilledRequests = new JButton("UNFILLED ");
		btnUnfilledRequests.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String a = SQLProvider.viewUnavailable();
					if(a == null)
					{
						runnable.run();
						JOptionPane.showMessageDialog(null, "There are no unfilled requests.");
						return;
					}
					else
					{
						
						textArea.setText("\t        Below shows the Unfilled Requests.\n");
						textArea.append("-------------------------------------------------------------"
								+ "-------------------------------------------------------------\n");

						textArea.append(a);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		btnUnfilledRequests.setBounds(197, 341, 111, 23);
		contentPane.add(btnUnfilledRequests);
		
		JButton btnNewButton = new JButton("NEW CAB");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Add_UI ui = new Add_UI();
				
				ui.setVisible(true);
				
			}
		});
		btnNewButton.setBounds(318, 341, 111, 23);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("FEEDBACK");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String a = SQLProvider.getFeedbackCarID(textField.getText());
					if(a == null)
					{
						runnable.run();
						JOptionPane.showMessageDialog(null, "Invalid Car ID.");
						return;
					}
					else
					{
						String b = SQLProvider.getFeedback();
						textArea.setText("\tBelow shows the Feedback of each Taxi Operator\n");
						textArea.append("-------------------------------------------------------------"
								+ "-------------------------------------------------------------\n");
						textArea.append("CarID\t\tRating\t\tComments\n");
						textArea.append("-------------------------------------------------------------"
								+ "-------------------------------------------------------------\n");
						textArea.append(b);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnNewButton_1.setBounds(123, 471, 111, 23);
		contentPane.add(btnNewButton_1);
		
		JButton btnDecom = new JButton("DECOM");
		btnDecom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String id = textField.getText();
				try {
					String a = SQLProvider.getCarID(id);
					
					if(a == null)
					{
						runnable.run();
						JOptionPane.showMessageDialog(null, "Invalid Car ID.");
						return;
					}
					else
					{
						SQLProvider.delete(id);
						textArea.setText("");
						textArea.append("-------------------------------------------------------------"
								+ "-------------------------------------------------------------\n");
						textArea.append("Decomission of Car ID: "+ id + " has been successful.\n");
						textArea.append("-------------------------------------------------------------"
								+ "-------------------------------------------------------------\n");
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				
			}
		});
		btnDecom.setBounds(276, 471, 111, 23);
		contentPane.add(btnDecom);
		
		textField = new JTextField();
		textField.setBounds(192, 428, 111, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblPleaseEnter = new JLabel("Please enter cab ID:");
		lblPleaseEnter.setBounds(174, 392, 134, 14);
		contentPane.add(lblPleaseEnter);
		
		JSeparator separator = new JSeparator();
		separator.setForeground(Color.LIGHT_GRAY);
		separator.setBounds(157, 417, 158, 14);
		contentPane.add(separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setForeground(Color.LIGHT_GRAY);
		separator_1.setBounds(48, 32, 399, 14);
		contentPane.add(separator_1);
		
		JLabel lblDatabaseQueries = new JLabel("Database Queries");
		lblDatabaseQueries.setBounds(182, 11, 131, 14);
		contentPane.add(lblDatabaseQueries);
	}


}
