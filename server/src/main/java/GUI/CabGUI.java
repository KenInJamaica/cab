package GUI;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

import java.util.Enumeration;
import java.awt.SystemColor;
import java.awt.Toolkit;

import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JTextField;


import cabDatabase.SQLProvider;

import javax.swing.JPasswordField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.JSeparator;
import java.awt.Color;

public class CabGUI extends JFrame implements KeyListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	JPanel p=new JPanel();
	
	
	private final JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
	private final JPanel login = new JPanel();
	private JTextField textField;
	private JPasswordField passwordField;
	

	
	public CabGUI(){
		
		setSize(511,578);
		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		p.setLayout(null);
		p.setBackground(SystemColor.control);
		getContentPane().add(p);
		tabbedPane.setBounds(12, 13, 481, 527);
		p.add(tabbedPane);
		
		tabbedPane.addTab("Login", null, login, null);
		login.setLayout(null);
		
		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setBounds(119, 107, 75, 14);
		login.add(lblUsername);
		
		textField = new JTextField();
		textField.setBounds(193, 104, 141, 20);
		login.add(textField);
		textField.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setBounds(119, 156, 66, 14);
		login.add(lblPassword);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(193, 153, 141, 20);
		login.add(passwordField);
		
		JButton btnLogin = new JButton("LOGIN");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String username = textField.getText();
			    char[] pass = passwordField.getPassword();
			    String password = String.valueOf(pass);  
			    
			    int a = SQLProvider.loginDatabase(username, password);
			    
			    if(a == 1){
					Database_GUI ui = new Database_GUI();
					
					ui.setVisible(true);
			        }
			    else{
			    	final Runnable runnable = (Runnable) Toolkit.getDefaultToolkit().getDesktopProperty("win.sound.exclamation");
			    	runnable.run();
			    	JOptionPane.showMessageDialog(null, "Incorrect Username/Password");
			    }
			    
			    textField.setText("");
			    passwordField.setText("");
			}

		});
		btnLogin.setBounds(193, 198, 89, 23);
		login.add(btnLogin);
		
		JPanel Service = new JPanel();
		tabbedPane.addTab("Customer Service", null, Service, null);
		Service.setLayout(null);
		
		final JTextArea textArea = new JTextArea();
		textArea.setBounds(10, 96, 456, 244);
		Service.add(textArea);
		
		JButton btnShow = new JButton("SHOW");
		btnShow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				    try {
				    	String a = SQLProvider.getHelp();
				    	if (a == null)
				    	{
							JOptionPane.showMessageDialog(null, "No queries found!");
							return;
				    	}else {
				    		textArea.setText(a);
							textArea.setText("\t\tCustomer Queries\n");
							textArea.append("-------------------------------------------------------------"
									+ "-------------------------------------------------------------\n");
							textArea.append("The details are as follows:\n");
							textArea.append("-------------------------------------------------------------"
									+ "-------------------------------------------------------------\n");
							textArea.append(a);
				    	}
						

					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} 
			}
		});
		btnShow.setBounds(193, 381, 89, 23);
		Service.add(btnShow);
		

		JLabel lblCustomerQueries = new JLabel("Customer Queries");
		lblCustomerQueries.setBounds(173, 46, 129, 14);
		Service.add(lblCustomerQueries);
		
		JSeparator separator = new JSeparator();
		separator.setBackground(Color.LIGHT_GRAY);
		separator.setBounds(98, 60, 279, 14);
		Service.add(separator);
		
		setVisible(true);
	}
	

	    public String getSelectedButtonText(ButtonGroup buttonGroup) {
	        for (Enumeration<AbstractButton> feed = buttonGroup.getElements(); feed.hasMoreElements();) {
	            AbstractButton button = feed.nextElement();

	            if (button.isSelected()) {
	                return button.getText();
	            }
	        }

	        return null;
	    }


		public void keyPressed(KeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}


		public void keyReleased(KeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}


		public void keyTyped(KeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}
}