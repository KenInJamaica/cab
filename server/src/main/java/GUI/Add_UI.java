package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.demo.basic.bean.Cabs;


import static cabDatabase.SQLProvider.*;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JSeparator;
import java.awt.Color;
import java.awt.event.ActionListener;

public class Add_UI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private static JTextField textField;
	private static JTextField textField_1;
	private static JTextField textField_2;
	private static JTextField textField_3;
	private static JTextField textField_4;
	private static JTextField textField_5;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Add_UI frame = new Add_UI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Add_UI() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 451, 368);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAddNewCab = new JLabel("Add New Cab");
		lblAddNewCab.setBounds(171, 11, 92, 14);
		contentPane.add(lblAddNewCab);
		
		JLabel lblCabh = new JLabel("Cab ID");
		lblCabh.setBounds(101, 49, 46, 14);
		contentPane.add(lblCabh);
		
		JLabel lblNameOfDriver = new JLabel("Name");
		lblNameOfDriver.setBounds(101, 86, 72, 14);
		contentPane.add(lblNameOfDriver);
		
		JLabel lblTrn = new JLabel("TRN");
		lblTrn.setBounds(101, 119, 46, 14);
		contentPane.add(lblTrn);
		
		JLabel lblMotorvehicleModel = new JLabel("Model");
		lblMotorvehicleModel.setBounds(101, 150, 51, 14);
		contentPane.add(lblMotorvehicleModel);
		
		JLabel lblYear = new JLabel("Year");
		lblYear.setBounds(101, 186, 46, 14);
		contentPane.add(lblYear);
		
		textField = new JTextField();
		textField.setBounds(190, 46, 123, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(190, 83, 123, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(190, 116, 123, 20);
		contentPane.add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setBounds(190, 147, 123, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(190, 178, 123, 20);
		contentPane.add(textField_4);
		
		JButton btnNewButton = new JButton("SUBMIT");
		btnNewButton.addActionListener(new Action());
		
		btnNewButton.setBounds(160, 264, 111, 41);
		contentPane.add(btnNewButton);
		
		JSeparator separator = new JSeparator();
		separator.setForeground(Color.LIGHT_GRAY);
		separator.setBounds(87, 36, 260, 8);
		contentPane.add(separator);
		
		JLabel lblAvailable = new JLabel("Available");
		lblAvailable.setBounds(101, 212, 46, 14);
		contentPane.add(lblAvailable);
		
		textField_5 = new JTextField();
		textField_5.setBounds(190, 209, 123, 20);
		contentPane.add(textField_5);
		textField_5.setColumns(10);
	}
	public static class Action implements ActionListener{

		public void actionPerformed(java.awt.event.ActionEvent evt) {
			String id = textField.getText();
			String name = textField_1.getText();
			String trn = textField_2.getText();
			String model = textField_3.getText();
			String year = textField_4.getText();
			String available = textField_5.getText();

			

			Cabs addCab = new Cabs(id, name, trn, model, year, available);
			//add a new cab
			try {
				post(addCab);
				textField.setText("");
				textField_1.setText("");
				textField_2.setText("");
				textField_3.setText("");
				textField_4.setText("");
				textField_5.setText("");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		
		}
	}
}