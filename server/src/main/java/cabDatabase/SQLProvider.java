package cabDatabase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.demo.basic.bean.Cabs;
import org.demo.basic.bean.Feedback;
import org.demo.basic.bean.Requests;

 public class SQLProvider {
	
	protected Connection con = null;
	protected Statement statement = null;
	protected ResultSet result = null;
	
	public static Connection getConnection() throws Exception{
		try{
			String driver = "com.mysql.cj.jdbc.Driver";
			String url = "jdbc:mysql://localhost:3306/cabss";
			String username = "root";
			String password = "root";
			Class.forName(driver);
			
			Connection conn = DriverManager.getConnection(url, username, password);
			
			
			return conn;
		} catch(Exception e){System.out.println(e);}
		
		
		return null;
	}
	
	public static void createTable() throws Exception{
		try{
			
			Connection con = getConnection();
			PreparedStatement create = con.prepareStatement("CREATE TABLE IF NOT EXISTS data(id varchar(10), name varchar(20), trn varchar(9), model varchar(50), year varchar(10), available varchar (1), PRIMARY KEY(id))");
			create.executeUpdate();	
			
			PreparedStatement createRequest = con.prepareStatement("CREATE TABLE IF NOT EXISTS summary(carid varchar(10), totalDist integer(20), totalAmt float(9), PRIMARY KEY(carid))");
			createRequest.executeUpdate();
			
			PreparedStatement createFeedback = con.prepareStatement("CREATE TABLE IF NOT EXISTS feedback(carid varchar(10), feedback varchar(20), message varchar(255), PRIMARY KEY(carid))");
			createFeedback.executeUpdate();
			
			PreparedStatement createLogin = con.prepareStatement("CREATE TABLE IF NOT EXISTS login(username varchar(20), password varchar(20))");
			createLogin.executeUpdate();
			
			
			
			
		}catch(Exception e){
			System.out.println(e);
		}

	}
		
	
	public static String getFeedbackCarID(String id) throws Exception{
		try{
			Connection con = getConnection();
			PreparedStatement statement = con.prepareStatement("SELECT * FROM feedback;");
			
			ResultSet result = statement.executeQuery();
			
			while(result.next()){
				if(result.getString(1).equals(id))
				{
					return result.getString(1);
				}
			}			
		}catch(Exception e){
			System.out.println(e);
			}
		return null;
	}
	
	public static int delete(String id ) {
		try{
			Connection con = getConnection();
			PreparedStatement statement = con.prepareStatement("DELETE FROM data WHERE id = '" +id+  "';");
			
			
			statement.executeUpdate();
					
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return 0;
	}
	
	public static String getCarID(String id) throws Exception{
		try{
			Connection con = getConnection();
			PreparedStatement statement = con.prepareStatement("SELECT * FROM data;");
			
			ResultSet result = statement.executeQuery();
			
			while(result.next()){
				if(result.getString(1).equals(id))
				{
					return result.getString(1);
				}
				
			}			
		}catch(Exception e){
			System.out.println(e);
			}
		return null;
	}
	
	public static String getAvailability() throws Exception{
		String a = null;
		try{
			Connection con = getConnection();
			PreparedStatement statement = con.prepareStatement("SELECT * FROM data WHERE available = '1'");
			
			ResultSet result = statement.executeQuery();
			Cabs item = null;
			while(result.next()){
				item = new Cabs();
				
				item.setCarID(result.getString(1)); // get ID column 1
				item.setAvailable(result.getString(6));
				a = result.getString(1);
			}
			
			
		}catch(Exception e){
			System.out.println(e);
			}
		return a;
	}

	public static void post(Cabs item) throws Exception{
		
		try{
			Connection con = getConnection();
			
			PreparedStatement posted = con.prepareStatement("INSERT INTO data(id, name, trn, model, year, available) "
					+ "VALUES ( '"+item.getCarID()+"', '"+item.getName()+"', '"+item.getTrn()+"', '"+item.getVehicleModel()+"', '"+item.getVehicleYear()+"', '"+item.getAvailable()+"')");
			posted.executeUpdate();
			
		} catch(Exception e){
			System.out.println(e);
			}
	}	
	
	public static void addRequest(String id, int distance, float amount) throws Exception{
		try{
			Connection con = getConnection();


			PreparedStatement posted = con.prepareStatement("INSERT INTO summary(carid, totalDist, totalAmt)  "
					+ "VALUES ( '"+id+"', '"+distance+"', '"+amount+"')");
			posted.executeUpdate();
			
		} catch(Exception e){
			System.out.println(e);
			}
	}
	
	public static void addFeedback(Feedback items) throws Exception{
		try{
			Connection con = getConnection();


			PreparedStatement posted = con.prepareStatement("INSERT INTO feedback(carid, feedback, message)  "
					+ "VALUES ( '"+items.getCarID()+"', '"+items.getRating()+"', '"+items.getMessage()+"')");
			posted.executeUpdate();
			
		} catch(Exception e){
			System.out.println(e);
			}
	}
	public static String getSummary(String a) throws Exception{
		try{
			Connection con = getConnection();
			String str ="", strApp, amtStr, distStr;
			PreparedStatement statement = con.prepareStatement("SELECT * FROM summary");
			
			ResultSet result = statement.executeQuery();
			Requests item = new Requests();	
			
			
			if(result.next()){ 	
			do
				{	
					item = new Requests();
					int dist = result.getInt(2);
					float amt = result.getFloat(3);
					
					item.setCarId(result.getString(1)); // get ID column 1
					item.setTotalDist(dist);
					item.setTotalAmt(amt);	
					//textArea.setText(item.getCarId() +" "+ item.getTotalAmt() +" "+ item.getTotalDist());
					
					distStr = Integer.toString(dist);
					amtStr = Float.toString(amt);
					strApp = result.getString(1) + "\t\t" + distStr + "\t\t" + amtStr + "\t" + " \n";
					
				str = str + strApp;	
				}while(result.next());
			
			}
			
			return str;
		}catch(Exception e){
			System.out.println(e);
			}
		return null;
	}
	
	public static String viewUnavailable() throws Exception{
		try{

			Connection con = getConnection();
				String string = null;
			PreparedStatement statement = con.prepareStatement("SELECT * FROM Unavail");
			
			ResultSet result = statement.executeQuery();
			
			if(result.next()){ 	
				do
					{					

						string = result.getString(1) + " \n";
					}while(result.next());
				
				}
				return string;		
			
		} catch(Exception e){
			System.out.println(e);
			}
		return null;
	}
	
	public static String getFeedback() throws Exception{
		try{
			Connection con = getConnection();
			String strApp = null;
			PreparedStatement statement = con.prepareStatement("SELECT * FROM feedback");
			
			ResultSet result = statement.executeQuery();			
			
			if(result.next()){ 	
			do
				{					

					strApp = result.getString(1) + "\t\t" + result.getString(2) + "\t\t" + result.getString(3) + "\t" + " \n";
				}while(result.next());
			
			}
			return strApp;
		}catch(Exception e){
			System.out.println(e);
			}
		return null;
	}
	
	public static int loginDatabase(String username, String password){
	    
	    String query = "select * from login";
	   
		try {
			Connection con = getConnection();
			PreparedStatement statement = con.prepareStatement(query);
		
		    //Execute
			ResultSet result = statement.executeQuery();
			if(result.next()){
			    if((result.getString(1).equals(username) && result.getString(2).equals(password)) == true)
			    {
			    	return 1;
			    }
			}
		    
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 2;

	}
	
	public static String getHelp() throws Exception{
		try{
			Connection con = getConnection();
			String strApp = "";
			PreparedStatement statement = con.prepareStatement("SELECT * FROM help");
			
			ResultSet result = statement.executeQuery();			
			
			if(result.next()){ 	
			do
				{					

					strApp = strApp + result.getString(1) + "\n" + result.getString(2) + "\n" + result.getString(3) + "\n" + result.getString(4) + "\n" + result.getString(5) + " \n\n";
				}while(result.next());
			
			}
			return strApp;
		}catch(Exception e){
			System.out.println(e);
			}
		return null;
	}
}
